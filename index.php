<?php
	include('base.php');

	// If database `epo` doesn't exists, then create it. 
	if($conn->connect_error){
		$sql = "create database ".$database.";";
        $conn->query($sql);
	}
	// If table `interviewex` doesn't exists, then create it.
	$sql = "select 1 from interviewex;";
	if(!($conn->query($sql))){
		$sql = "create table interviewex(
			id int(4) primary key auto_increment,
			organization varchar(20),
			eng_stream varchar(20),
			app_mode varchar(10),
			select_pro int(1),
			tech_int int(1),
			ana_q int(1),
			hr_q int(1),
			suggest int(1),
			info varchar(200)
		);";
        $conn->query($sql);
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>ExamPreparationOnline</title>
    <?php include('header.php'); ?>
	<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                margin-top: -20%;
            }

            h1 {
                font-size: 650%;
                margin-bottom: 10%;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

        </style>

</head>
<body>
	<div class="container">
		<div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <h1>ExamPreparationOnline</h1>
                </div>

                <div class="links">
                    <a href="submit.php">Submit</a>
                    <a href="view.php">View</a>
                </div>
            </div>
        </div>
	</div>
</body>
</html>