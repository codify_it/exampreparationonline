<?php
	include('base.php');

	$sql = "SELECT * FROM interviewex ORDER BY id DESC;";
	$result = $conn->query($sql);

	if($result->num_rows > 0){
		while($row = $result->fetch_assoc()){
			$ctr = FALSE;
			$start =  '<div class="col-md-offset-2 col-md-8" style="border: 1px black solid; border-radius: 20px; text-align: center; margin-bottom: 2%;">';
			$end = '</div>';

			$row_start = '<div class="row" style="margin-top: 0.3%;">';
			$row_end = '</div>';

			$data = $row_start.'<strong>Organization: </strong> '.$row['organization'].$row_end;
			$data = $row_start.$data.'<strong>Engineering Stream: </strong> '.$row['eng_stream'].$row_end;
			$data = $row_start.$data.'<strong>Application Mode: </strong> '.$row['app_mode'].$row_end.'<br>';
			$data = $row_start.$data."<strong>Selected Options</strong>".$row_end;
			if($row['select_pro'] == 1){
				$ctr=TRUE;
				$data = $row_start.$data.'<strong>Selection Procedure</strong>'.$row_end;
			}
			if($row['tech_int'] == 1){
				$ctr=TRUE;
				$data = $row_start.$data.'<strong>Technical Interview</strong>'.$row_end;
			}
			if($row['ana_q'] == 1){
				$ctr=TRUE;
				$data = $row_start.$data.'<strong>Analytical Questions</strong>'.$row_end;
			}
			if($row['hr_q'] == 1){
				$ctr=TRUE;
				$data = $row_start.$data.'<strong>HR Questions</strong>'.$row_end;
			}
			if($row['suggest'] == 1){
				$ctr=TRUE;
				$data = $row_start.$data.'<strong>Suggestions</strong>'.$row_end;
			}
			if(!$ctr){
				$data = $row_start.$data.'<strong>None!</strong>'.$row_end;
			}
			$data = $row_start.$data."<br><strong>Information Provided</strong>".$row_end;
			$data = $row_start.$data.$row['info'].$row_end.'<br>';

			echo $start.$data.$end;
			
		}
	}
	else{
		echo '<div class="alert alert-info" style="margin-top: 25%; text-align: center; border-radius: 15px;">
  			<h3><strong>Empty!</strong>No Submissions Yet...</h3>
		</div>';
	}
?>