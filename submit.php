<?php
	include('base.php');
	
	if(isset($_POST['org'])){
		$org = $_POST['org'];
		$engg = $_POST['engg'];
		$app_mode = $_POST['app_mode'];

		$details = mysqli_real_escape_string($conn,$_POST['details']);

		$sp = $_POST['sel_pro'];
		$ti = $_POST['tech_int'];
		$aq = $_POST['analyt_q'];
		$hrq = $_POST['hrq'];
		$s = $_POST['suggest'];

		if($sp != "on") $sp = 0;
		else $sp = 1;
		if($ti != "on") $ti = 0;
		else $ti = 1;
		if($aq != "on") $aq = 0;
		else $aq = 1;
		if($hrq != "on") $hrq = 0;
		else $hrq = 1;
		if($s != "on") $s = 0;
		else $s = 1;

		$sql = "INSERT into interviewex(organization,eng_stream,app_mode,select_pro,tech_int,ana_q,hr_q,suggest,info) VALUES('$org','$engg','$app_mode',$sp,$ti,$aq,$hrq,$s,'$details');";
		mysqli_query($conn,$sql);
		header("Location: successful.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Submit</title>
	<?php include('header.php'); ?>
	<link rel="stylesheet" type="text/css" href="css/submit.css">
	<style type="text/css">
		
		li a{
			text-decoration: none;
			color: #636b6f;
			border : 1px #636b6f solid;
			border-radius: 4px;
		}
		li a:hover{
			text-decoration: none;
			color: #000;
			background-color: #fff;
		}

		.navbar-brand
		{
		    position: absolute;
		    width: 100%;
		    left: 0;
		    text-align: center;
		    margin:0 auto;
		}
		.navbar-toggle {
		    z-index:3;
		}
	</style>
</head>
<body>
	<div class="container">
		<br>
		<div class="row">
			<nav class="navbar" role="navigation">
				<div class="navbar-collapse collapse">
				  	<ul class="nav navbar-nav navbar-left">
				      	<li><a href="/">Home</a></li>
				  	</ul>
				  	<ul class="nav navbar-nav navbar-right">
				    	<li><a href="/view.php">View</a></li>
				  	</ul>
				</div>
			</nav>
		</div>
		<form method="POST" action="">
			<div class="form-group">
				<div class="col-md-12">
					<div class="row">
						<div class="jumbotron col-md-4" style="align-items: center;">
							<div class="row">
								<label for="org">Organisation</label>
								<select class="form-control dropdown" id="org" name="org">
							        <option value="Microsoft">Microsoft</option>
							        <option value="Google">Google</option>
							        <option value="Facebook">Facebook</option>
							        <option value="Apple">Apple</option>
							    </select>
							</div>
							<br>
							<div class="row">
								<label for="engg">Engineering Stream</label>
								<select class="form-control dropdown" id="engg" name="engg">
							        <option value="CSE">CSE</option>
							        <option value="ECE">ECE</option>
							        <option value="IT">IT</option>
							        <option value="Mechanical">Mechanical</option>
							    </select>
							</div>
							<br>
							<div class="row">
								<label for="app_mode">Application Mode</label>
								<select class="form-control dropdown" id="app_mode" name="app_mode">
							        <option value="Off campus">Off Campus</option>
							        <option value="On Campus">On Campus</option>
							    </select>
							</div>
						</div>
						<div class="col-md-1 line"></div>
						<div class="col-md-offset-1 col-md-6" style="margin-top: 5%">
							<label for="details">Details</label>
							<textarea class="form-control" rows="15" id="details" name="details"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="jumbotron col-md-4 bot" style="align-items: center;">
							<div class="row">
								<div class="checkbox">
									<input type="checkbox" name="sel_pro" id="sel_pro">
									<label for="sel_pro">Selection Procedure</label>
								</div>
								<div class="checkbox">
									<input type="checkbox" name="tech_int" id="tech_int">
									<label for="tech_int">Technical Interview</label>
								</div>
								<div class="checkbox">
									<input type="checkbox" name="analyt_q" id="analyt_q">
									<label for="analyt_q">Analytical Questions</label>
								</div>
								<div class="checkbox">
									<input type="checkbox" name="hrq" id="hrq">
									<label for="hrq">HR Questions</label>
								</div>
								<div class="checkbox">
									<input type="checkbox" name="suggest" id="suggest">
									<label for="suggest">Suggestions</label>
								</div>
							</div>
						</div>
						<div class="col-md-offset-5 col-md-6 submit" style="margin-top: -15%;">
							<input type="submit" name="submit" class="btn btn-primary" value="Submit" style="width: 100%;">
						</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>