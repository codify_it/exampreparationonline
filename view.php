<!DOCTYPE html>
<html>
<head>
	<title>Submissions</title>
	<?php include('header.php'); ?>
	<style type="text/css">
		
		li a{
			text-decoration: none;
			color: #636b6f;
			border : 1px #636b6f solid;
			border-radius: 4px;
		}
		li a:hover{
			text-decoration: none;
			color: #000;
			background-color: #fff;
		}

		.navbar-brand
		{
		    position: absolute;
		    width: 100%;
		    left: 0;
		    text-align: center;
		    margin:0 auto;
		}
		.navbar-toggle {
		    z-index:3;
		}
	</style>
</head>
<body>
	<div class="container">
		<br>
		<nav class="navbar" role="navigation">
			<div class="navbar-collapse collapse">
			  	<ul class="nav navbar-nav navbar-left">
			      	<li><a href="/">Home</a></li>
			  	</ul>
			  	<ul class="nav navbar-nav navbar-right">
			    	<li><a href="/submit.php">Submit</a></li>
			  	</ul>
			</div>
		</nav>
		<?php
			include('display.php');
		?>
	</div>
</body>
</html>