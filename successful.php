<!DOCTYPE html>
<html>
<head>
	<title>Success</title>
	<?php include('header.php'); ?>
    <script type="text/javascript">
    	function redirect(){
    		window.location ='index.php';
    	}
    </script>
</head>
<body onload="setTimeout(redirect,2500);">
	<div class="container">
		<div class="alert alert-success" style="margin-top: 25%; text-align: center; border-radius: 15px;">
  			<h1><strong>Success!</strong> redirecting to homepage. Please wait...</h1>
		</div>
	</div>
</body>
</html>